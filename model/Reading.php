<?php
  class Reading{
    // DB Stuff
    private $conn;
    private $table = 'sensor_data';

    // Post Properties
    public $id;
    public $value1;
    public $value2;
    public $value3;
    public $label;
    public $is_processed;
    public $created_on;

    // Constructor
    public function __construct($db){
      $this->conn = $db;
    }

    // Get All Posts
    public function read(){
      $query = 'SELECT * FROM ' . $this->table;

      $stmt = $this->conn->prepare($query); // Prepare statement
      $stmt->execute(); // Execute query

      return $stmt;
    }

    // Get Single Post
    public function readSingle(){
      $query = 'SELECT * FROM
          ' . $this->table . ' p WHERE p.id = ? LIMIT 0,1';

      $stmt = $this->conn->prepare($query); // Prepare statement
      $stmt->bindParam(1, $this->id);
      $stmt->execute(); // Execute query

      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      $this->value1 = $row['value1'];
      $this->value2 = $row['value2'];
      $this->value3 = $row['value3'];
      $this->label = $row['label'];
      $this->created_on = $row['created_on'];

      return $stmt;
    }

    public function create(){
      $query = 'INSERT INTO '.$this->table.'
       SET
       value1 = :value1,
       value2 = :value2,
       value3 = :value3,
       label = :label';
      $stmt = $this->conn->prepare($query);

      // Clean and Bind Data
      $this->value1 = htmlspecialchars(strip_tags($this->value1));
      $this->value2 = htmlspecialchars(strip_tags($this->value2));
      $this->value3 = htmlspecialchars(strip_tags($this->value3));
      $this->label = htmlspecialchars(strip_tags($this->label));

      $stmt->bindParam(':value1', $this->value1);
      $stmt->bindParam(':value2', $this->value2);
      $stmt->bindParam(':value3', $this->value3);
      $stmt->bindParam(':label', $this->label);

      // Execture query
      if($stmt->execute()){
        return true;
      }

      // Print error if something goes wrong
      printf("Error: %s.\n", $stmt->error);
      return false;
    }

    public function update_label(){
      $query = 'UPDATE '.$this->table.'
        SET
          label = :label
        WHERE id = :id';
        // ? = positional parameter; :X = named parameter

      $stmt = $this->conn->prepare($query);

      // Clean and Bind Data
      $this->id = htmlspecialchars(strip_tags($this->id));
      //$this->value1 = htmlspecialchars(strip_tags($this->value1));
      //$this->value2 = htmlspecialchars(strip_tags($this->value2));
      $this->label = htmlspecialchars(strip_tags($this->label));
      $stmt->bindParam(':id', $this->id);
      //$stmt->bindParam(':value1', $this->value1);
      //$stmt->bindParam(':value2', $this->value2);
      $stmt->bindParam(':label', $this->label);

      // Execture query
      if($stmt->execute()){
        return true;
      }

      // Print error if something goes wrong
      printf("Error: %s.\n", $stmt->error);
      return false;
    }

    public function update_is_processed(){
      $query = 'UPDATE '.$this->table.'
        SET
          is_processed = :is_processed
        WHERE id = :id';
        // ? = positional parameter; :X = named parameter

      $stmt = $this->conn->prepare($query);

      // Clean and Bind Data
      $this->id = htmlspecialchars(strip_tags($this->id));
      $this->is_processed = htmlspecialchars(strip_tags($this->is_processed));
      $stmt->bindParam(':id', $this->id);
      $stmt->bindParam(':is_processed', $this->is_processed);

      // Execture query
      if($stmt->execute()){
        return true;
      }

      // Print error if something goes wrong
      printf("Error: %s.\n", $stmt->error);
      return false;
    }

    public function delete(){
      $query = 'DELETE FROM '.$this->table.' WHERE id = :id';
      $stmt = $this->conn->prepare($query);

      // Clean Data
      $this->id = htmlspecialchars(strip_tags($this->id));

      // Bind Data (id using named parameter)
      $stmt->bindParam(':id', $this->id);

      // Execture query
      if($stmt->execute()){
        return true;
      }

      // Print error if something goes wrong
      printf("Error: %s.\n", $stmt->error);
      return false;
    }
  }
 ?>
