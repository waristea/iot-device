<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: PUT');
  //header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

  include_once '../../config/Database.php';
  include_once '../../model/Reading.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  $reading = new Reading($db); // Instantiate object
  // Get raw posted Data
  $data = json_decode(file_get_contents("php://input"));

  if ($data->label!=NULL | $data->label===0) {
    for($i=$data->id[0]; $i<=$data->id[1]; $i++){
        $reading->id = $i;
        $reading->label = $data->label;

        if (!$reading->update_label()) {
          echo json_encode( array('message'=> 'Readings not Updated!'));
        }
    }
  }

  if ($data->is_processed!=NULL | $data->is_processed===0) {
    for($i=$data->id[0]; $i<=$data->id[1]; $i++){
        $reading->id = $i;
        $reading->is_processed = $data->is_processed;

        if (!$reading->update_is_processed()) {
          echo json_encode( array('message'=> 'Readings not Updated!'));
        }
    }
  }

  //echo json_encode( array('message'=> $data->value2));
  echo json_encode( array('message'=> 'Readings Updated!'));

?>
