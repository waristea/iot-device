<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../model/Reading.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate object
  $reading = new Reading($db);
  $reading->id = isset($_GET['id']) ? $_GET['id'] : die();
  $reading->readSingle();

  extract($reading);
  $reading_item = array(
    'id' => $reading->id,
    'value1' => $reading->value1,
    'value2' => $reading->value2,
    'value3' => $reading->value3,
    'label' => $reading->label,
    'created_on' => $reading->created_on
  );
  // Turn to json
  echo json_encode($reading_item);
 ?>
