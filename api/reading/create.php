<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  //header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

  include_once '../../config/Database.php';
  include_once '../../model/Reading.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  $reading = new Reading($db); // Instantiate object

  // Get raw posted Data
  $data = json_decode(file_get_contents("php://input"));
  $reading->value1 = $data->value1;
  $reading->value2 = $data->value2;
  $reading->value3 = $data->value3;
  $reading->label = $data->label;

  // Create posts
  if($reading->create()){
    echo json_encode( array('message'=> 'Reading Created'));
  } else {
    echo json_encode( array('message'=> 'Reading Not Created'));
  }
?>
