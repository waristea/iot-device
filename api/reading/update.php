<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: PUT');
  //header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

  include_once '../../config/Database.php';
  include_once '../../model/Reading.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  $reading = new Reading($db); // Instantiate object

  // Get raw posted Data
  $data = json_decode(file_get_contents("php://input"));
  $reading->id = $data->id;
  //$reading->value1 = $data->value1;
  //$reading->value2 = $data->value2;
  $reading->label = $data->label;

  // Update reading
  if($reading->update()){
    echo json_encode( array('message'=> 'Reading Updated'));
  } else {
    echo json_encode( array('message'=> 'Reading Not Updated'));
  }
?>
