<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  //header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

  include_once '../../config/Database.php';
  include_once '../../model/Reading.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  $reading = new Reading($db); // Instantiate object
  // Get raw posted Data
  $data = json_decode(file_get_contents("php://input"));

  //echo json_encode( array('message'=> gettype($data->length)));
  //echo json_encode( array('message'=> $data->values));

  //echo json_encode( array('message'=> $data->length));

  for($i=0; $i<$data->length; $i++){
      $reading->value1 = $data->value1[$i];
      $reading->value2 = $data->value2[$i];
      $reading->value3 = $data->value3[$i];
      $reading->label = $data->label[$i];

      //echo json_encode( array('message'=> $reading->value1));
      //echo json_encode( array('message'=> $reading->value2));
      if (!$reading->create()) {
        echo json_encode( array('message'=> 'Bulk Reading Not Created'));
      }
  }

  //echo json_encode( array('message'=> $data->value2));
  echo json_encode( array('message'=> 'Bulk Reading Created!'));

?>
