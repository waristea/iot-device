<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../model/Reading.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  $reading = new Reading($db); // Instantiate object
  $result = $reading->read(); // Object query

  // Check if any reading
  if($result->rowCount() > 0){
    // reading array
    $readings_arr = array();
    $readings_arr['data'] = array();

    while($row = $result->fetch(PDO::FETCH_ASSOC)){
      extract($row);
      $reading_item = array(
        'id' => $id,
        'value1' => $value1,
        'value2' => $value2,
        'value3' => $value3,
        'label' => $label,
        'created_on' => $created_on,
      );

      array_push($readings_arr['data'], $reading_item); // Push to "data"
    };

    echo json_encode($readings_arr); // Turn to json

  } else{
    echo json_encode(
      array('message' => 'No post found')
    );
  }
 ?>
