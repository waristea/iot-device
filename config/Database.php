<?php

  class Database {
    private $host = 'localhost';
    private $db_name = 'iot_device_simple_triple';
    private $port = 3306;
    private $username = 'root';
    private $password = '1234567';
    private $conn;

    public function connect(){
      $this->conn = null;

      try{
        $this->conn = new PDO(
          'mysql:host='.$this->host.
          ';port='.$this->port.
          ';dbname='.$this->db_name,
          $this->username, $this->password);

        //$statement = $this->conn->query('SHOW DATABASES');
        //print_r( $statement->fetchAll() );

        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch(PDOException $e){
        echo 'Connection error : ' . $e->getMessage();
      }

      return $this->conn;
    }
  }
?>
